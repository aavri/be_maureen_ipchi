package com.aavri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BranchServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BranchServicesApplication.class, args);
	}

}

