package com.aavri.domain;

import java.io.Serializable;

public class Branch implements Serializable {
	private static final long serialVersionUID = 4768312137223786991L;

	private String branchName;
	private double latitude;
	private double longitude;
	private String streetAddress;
	private String city;
	private String countrySubDivision;
	private String country;
	private String postCode;

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountrySubDivision() {
		return countrySubDivision;
	}
	public void setCountrySubDivision(String countrySubDivision) {
		this.countrySubDivision = countrySubDivision;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
}
