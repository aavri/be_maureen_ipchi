package com.aavri.service;

import java.util.List;

import com.aavri.domain.Branch;

public interface BranchSvc {

	public List<Branch> getAllBranches();
	
	public List<Branch> getBranchByCity(String cityFilter);
}
