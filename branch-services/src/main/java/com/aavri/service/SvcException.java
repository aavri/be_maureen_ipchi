package com.aavri.service;

public class SvcException extends RuntimeException {
	private static final long serialVersionUID = -4056877081109899791L;

	private String msgCode;
	
	public SvcException(String msg, String msgCode) {
		super(msg);
		this.msgCode = msgCode;
	}
	
	public String getMsgCode() {
		return this.msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}	
}
