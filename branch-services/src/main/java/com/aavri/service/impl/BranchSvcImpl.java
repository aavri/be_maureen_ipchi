package com.aavri.service.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aavri.domain.Branch;
import com.aavri.service.BranchSvc;
import com.aavri.service.SvcException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

@Service
public class BranchSvcImpl implements BranchSvc {
	private final static String PROC_ENDPOINT = "https://api.halifax.co.uk/open-banking/v2.2/branches";
	
	private Logger logger = LoggerFactory.getLogger(BranchSvcImpl.class);
	
	@Autowired private HttpClient httpClient;

	@Override
	public List<Branch> getAllBranches() {
		return queryBranchesByCity(null);
	}

	/**
	 * Searches branches by city name (full or partial) and it is case insensitive
	 */
	@Override
	public List<Branch> getBranchByCity(String cityFilter) {
		if (StringUtils.isBlank(cityFilter)) {
			throw new SvcException("Your search criteria for city cannot be blank. Please enter a valid name.", "INVALID.PARAM");
		}
		
		return queryBranchesByCity(cityFilter);
	}
	
	private List<Branch> queryBranchesByCity(String cityFilter) {
		GetMethod getMethod = null;
		try {
			getMethod = new GetMethod(PROC_ENDPOINT);
			getMethod.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			
			int statusCode = httpClient.executeMethod(getMethod);
			logger.info("HTTP status Code: " + statusCode);
			
			if (statusCode != HttpStatus.SC_OK) {
				throw new SvcException("We were unable to retrieve branch list. Please try again later.", "REQUEST.FAILED");
			}
			
			try (InputStreamReader sr = new InputStreamReader(getMethod.getResponseBodyAsStream());) {
				JsonFactory jsonFactory = new JsonFactory();
				JsonParser jsonParser = jsonFactory.createParser(sr);	
				List<Branch> result = parseResponse(jsonParser, cityFilter);

				return result;
			}	
		}
		catch(IOException | SvcException e) {
			throw new SvcException("An exception occurred when retrieving branches", "REQUEST.FAILED");
		}
		finally {
			if (getMethod != null) {
				getMethod.releaseConnection();
			}
		}
	}
	
	private List<Branch> parseResponse(JsonParser jsonParser, String cityFilter) throws IOException {		
		JsonToken current = jsonParser.nextToken();
		String fieldName = null;

	    if (current != JsonToken.START_OBJECT) {
	    	logger.error("Invalid JSON response");
	    	return null;
	    }

		List<Branch> branches = new ArrayList<Branch>();
		cityFilter = (cityFilter != null) ? cityFilter.trim().toLowerCase() : null;

		while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
	    	fieldName = jsonParser.getCurrentName();
	    	current = jsonParser.nextToken();
	    }
   
		current = jsonParser.nextToken();
    	current = jsonParser.nextToken();
    	fieldName = jsonParser.getCurrentName();
    	
    	if ("data".equals(fieldName) && current == JsonToken.START_ARRAY) {
			while (!"Branch".equals(fieldName)) {
				fieldName = jsonParser.getCurrentName();
		    	current = jsonParser.nextToken();
			}
       			
    		Branch branch = new Branch();

    		if ("Branch".equals(fieldName) && current == JsonToken.START_ARRAY)  {
    			while(!jsonParser.isClosed()) {
    				if ("CustomerSegment".equals(fieldName) || "OtherServiceAndFacility".equals(fieldName)
    						|| "Availability".equals(fieldName) || "ContactInfo".equals(fieldName) ||
    						"Type".equals(fieldName) || "SequenceNumber".equals(fieldName) || "Identification".equals(fieldName)) {
    						
    					jsonParser.skipChildren();
    				}
					else if ("Name".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setBranchName(jsonParser.getValueAsString());
					}
					else if ("AddressLine".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setStreetAddress(jsonParser.getValueAsString());
					}
					else if ("TownName".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setCity(jsonParser.getValueAsString());
					}
					else if ("CountrySubDivision".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setCountrySubDivision(jsonParser.getValueAsString());
					}
					else if ("Country".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setCountry(jsonParser.getValueAsString());
					}
					else if ("PostCode".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setPostCode(jsonParser.getValueAsString());
					}
					else if ("Latitude".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setLatitude(Double.valueOf(jsonParser.getValueAsString()));
					}
					else if ("Longitude".equals(fieldName) && current == JsonToken.VALUE_STRING) {
						branch.setLongitude(Double.valueOf(jsonParser.getValueAsString()));
					}
					else if ("PostalAddress".equals(fieldName) && current == JsonToken.END_OBJECT) {
						if (cityFilter == null || cityFilter.equalsIgnoreCase(branch.getCity()) 
								|| (branch.getCity() != null && branch.getCity().toLowerCase().contains(cityFilter))) {
							branches.add(branch);
						}
						branch = new Branch();
					}
 
    				fieldName = jsonParser.getCurrentName();
        		    current = jsonParser.nextToken();
    			}
    		}
    	}
    	
		return branches;
	}
}
