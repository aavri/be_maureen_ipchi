package com.aavri.controller;

public class ExceptionMsg {
	private String msgCode;
	private String msgText;
	
	public ExceptionMsg() { }
	public ExceptionMsg(String msgCode, String msgText) {
		this.msgCode = msgCode;
		this.msgText = msgText;
	}
	
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgText() {
		return msgText;
	}
	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}
}
