package com.aavri.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.aavri.service.SvcException;

@RestControllerAdvice
public class ControllerExceptionAdvice {
	@ExceptionHandler(SvcException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	public ExceptionMsg handleException(SvcException e) {
		ExceptionMsg dto = new ExceptionMsg();
		dto.setMsgCode(e.getMsgCode());
		dto.setMsgText(e.getMessage());
		return dto;
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ExceptionMsg handleException(Exception e) {
		e.printStackTrace();
		ExceptionMsg dto = new ExceptionMsg();
		dto.setMsgCode("SYS_ERROR");
		dto.setMsgText("Unexpected System Error");
		return dto;
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ExceptionMsg handleValidationException(MissingServletRequestParameterException e) {
		ExceptionMsg msg = new ExceptionMsg();
		msg.setMsgCode(e.getParameterName());
		msg.setMsgText(e.getMessage());
		return msg;
	}
}
