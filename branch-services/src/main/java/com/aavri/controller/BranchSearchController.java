package com.aavri.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aavri.domain.Branch;
import com.aavri.service.BranchSvc;

@RestController
public class BranchSearchController {
	@Autowired private BranchSvc branchSvc;
	
    @RequestMapping(value="/branches", method={RequestMethod.GET, RequestMethod.POST})
    public List<Branch> listAllBranches() {
    	return branchSvc.getAllBranches();
    }
    
    @RequestMapping(value="/branch/searchbycity", method=RequestMethod.POST)
    public List<Branch> searchBranchByCity(
    		@RequestParam(value="city", required=true) String city) {
    	return branchSvc.getBranchByCity(city);
    }
}
