package com.aavri.springconfig;

import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.commons.httpclient.HttpClient;

@Configuration
public class HttpClientConfig {
	@Value("${httpclient.httpConnManager.maxTotalConnections:20}") private int maxTotalConnections;
	@Value("${httpclient.httpConnManager.defaultMaxConnectionsPerHost:1}") private int defaultMaxConnectionsPerHost;
	@Value("${httpclient.httpConnManager.soTimeout:50000}") private int soTimeout;
	@Value("${httpclient.httpConnManager.connectionTimeout:5000}") private int connectionTimeout;

	@Bean 
    public MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager() {
		MultiThreadedHttpConnectionManager connManager = new MultiThreadedHttpConnectionManager();
		connManager.getParams().setMaxTotalConnections(maxTotalConnections);
		connManager.getParams().setDefaultMaxConnectionsPerHost(defaultMaxConnectionsPerHost);
		connManager.getParams().setSoTimeout(soTimeout);
		connManager.getParams().setConnectionTimeout(connectionTimeout);
        return connManager; 
    }
	
	@Bean(name="httpClient")
    public HttpClient httpClient() { 
        return new HttpClient(multiThreadedHttpConnectionManager()); 
    }
}
