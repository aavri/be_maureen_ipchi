package com.aavri.testutils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.aavri.BranchServicesApplication;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

@ActiveProfiles(inheritProfiles=false,profiles="test")
@SpringBootTest(classes=BranchServicesApplication.class)
@AutoConfigureMockMvc
public @interface ControllerTestSettings { }
