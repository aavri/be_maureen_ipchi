package com.aavri.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
public class BaseControllerTest {
	@Autowired protected MockMvc mockMvc;
	
	protected boolean onetimeSetupDone = false;
	
	protected MockHttpServletRequestBuilder buildPost(String uri) {
		return MockMvcRequestBuilders.post(uri)
			.contentType(MediaType.APPLICATION_FORM_URLENCODED);
	}
	
	protected MockHttpServletRequestBuilder buildGet(String uri) {
		return MockMvcRequestBuilders.get(uri);
	}
	
	protected static String extractJsonValue(String jsonString, String attrName) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> resultMap = mapper.readValue(jsonString, new TypeReference<Map<String,String>>(){});
		return resultMap.get(attrName);
	}
}
