package com.aavri.controller;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import com.aavri.domain.Branch;
import com.aavri.testutils.ControllerTestSettings;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@ControllerTestSettings
public class BranchSearchControllerTest extends BaseControllerTest {
	
	@Test
	public void testGetAllBranches() {
		try {
			MvcResult result = mockMvc.perform(buildGet("/branches"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andReturn();
						
			String jsonString = result.getResponse().getContentAsString();
			ObjectMapper mapper = new ObjectMapper();
			List<Branch> branchList = mapper.readValue(jsonString, new TypeReference<List<Branch>>(){});

			assertTrue(branchList.size() >= 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("exception encountered: " + e.getMessage());
		}
	}
	
	@Test
	public void testSearchBranchByCityNameLowercase() {
		try {
			MvcResult result = mockMvc.perform(buildPost("/branch/searchbycity")
					.param("city", "liverpool"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andReturn();
						
			String jsonString = result.getResponse().getContentAsString();
			ObjectMapper mapper = new ObjectMapper();
			List<Branch> branchList = mapper.readValue(jsonString, new TypeReference<List<Branch>>(){});

			assertTrue(branchList.size() >= 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("exception encountered: " + e.getMessage());
		}
	}
	
	@Test
	public void testSearchBranchByCityNameUppercase() {
		try {
			MvcResult result = mockMvc.perform(buildPost("/branch/searchbycity")
					.param("city", "LIVERPOOL"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andReturn();
						
			String jsonString = result.getResponse().getContentAsString();
			ObjectMapper mapper = new ObjectMapper();
			List<Branch> branchList = mapper.readValue(jsonString, new TypeReference<List<Branch>>(){});

			assertTrue(branchList.size() >= 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("exception encountered: " + e.getMessage());
		}
	}
	
	@Test
	public void testSearchBranchByCityNamePartial() {
		try {
			MvcResult result = mockMvc.perform(buildPost("/branch/searchbycity")
					.param("city", "LOND"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andReturn();
						
			String jsonString = result.getResponse().getContentAsString();
			ObjectMapper mapper = new ObjectMapper();
			List<Branch> branchList = mapper.readValue(jsonString, new TypeReference<List<Branch>>(){});

			assertTrue(branchList.size() >= 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("exception encountered: " + e.getMessage());
		}
	}
	
	@Test
	public void testSearchBranchByCityNameWithSpace() {
		try {
			MvcResult result = mockMvc.perform(buildPost("/branch/searchbycity")
					.param("city", "PRESCOT "))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andReturn();
						
			String jsonString = result.getResponse().getContentAsString();
			ObjectMapper mapper = new ObjectMapper();
			List<Branch> branchList = mapper.readValue(jsonString, new TypeReference<List<Branch>>(){});

			assertTrue(branchList.size() >= 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("exception encountered: " + e.getMessage());
		}
	}
	
	@Test
	public void testSearchBranchByCityInvalidFilter() {
		try {
			mockMvc.perform(buildPost("/branch/searchbycity")
					.param("city", ""))
				.andDo(print())
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("msgCode").exists())
				.andExpect(jsonPath("msgText").exists())
				.andReturn();
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("exception encountered: " + e.getMessage());
		}
	}
}
